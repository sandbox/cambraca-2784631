(function($) {
  CKEDITOR.plugins.add( 'appliance', {
    requires: [ 'dialog' ],

    init: function( editor )
    {
      CKEDITOR.dialog.add( 'appliance', this.path + 'dialogs/appliance.js' );

      editor.addCommand( 'appliance', new CKEDITOR.dialogCommand( 'appliance' ) );
      // {
      //   exec : function( editor ) {
      //     here is where we tell CKEditor what to do.
          // editor.insertHtml( 'This text is inserted when clicking on our new button from the CKEditor toolbar' );
        // }
      // });

      editor.addCss('div.appliance {' +
        'margin: 5px;' +
        'padding: 5px;' +
        'border: 2px solid #666;' +
        'background: #999;' +
        'color: #fff;' +
        'border-radius: 2px;' +
        '}');

      editor.ui.addButton( 'appliance', {
        label: 'Add an appliance',
        command: 'appliance',
        icon: this.path + 'icon.png'
      });
    }
  });
})(jQuery);
