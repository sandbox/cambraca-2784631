(function($) {

  Drupal.behaviors.appliance = {
    attach: function (context, settings) {
      if (typeof Drupal.settings.appliances === 'undefined') {
        return;
      }

      for (var instance_id in Drupal.settings.appliances) {
        $('#appliance-' + instance_id, context).once('appliance', function() {
          var appliance_id = Drupal.settings.appliances[instance_id]['id'];
          var params = Drupal.settings.appliances[instance_id]['params'];
          Drupal.appliances[appliance_id]($, $('#appliance-' + instance_id), params);
        });
      }
    }
  };

})(jQuery);
